import React from "react";
import { groupBy } from 'lodash'
import moment from 'moment'
import { LineChart, XAxis, YAxis, CartesianGrid, Tooltip, Line, ResponsiveContainer } from 'recharts'

import "./App.css";
import mock from './mock'

const images = {
  uv: '651aa533-1811-4eb8-a128-1778dba4c404',
  pv: '57be4637-e6c7-49c2-aaf8-edbc7dfe0409'
}

const flattenedLines = Object.keys(mock).reduce((acc, curr) => {
  const lines = mock[curr]

  return [...acc, ...lines]
}, [])

const groupedLinesByDate = groupBy(flattenedLines, 'tim')
const dates = Object.keys(groupedLinesByDate).sort((a, b) => a - b)

const endData = dates.map((current) => {
  const time = moment(Number(current)).format('hh:mma')
  const [first, second] = groupedLinesByDate[current]
  return {
    time,
    uv: first.ap,
    pv: second.ap
  }
})

console.log('endData', endData)

const CustomTooltip  = (props) => {
  const { active } = props;

  if (active) {
    const { payload } = props;

    return (
      payload.map((item, index) => 
      <div key={index} style={{ backgroundColor: `${item.color}` }} className={`custom-tooltip`}><p>{`${yAxisFormatter(item.value)}`}</p></div>)
    );
  }

  return null;
};

function yAxisFormatter(value) {
  return value > 0 ? `+${value}` : value
}

const YAxisTick = (props) => {
  const { x, y, payload: { value }} = props
  const formattedValue = value > 0 ? `+${value}` : value
  
  return <text x={x / 2} y={y + 10} fill="#666" textAnchor="middle" dy={-6}>{formattedValue}</text>;
}

const CustomizedDot = (props) => {
  const {cx, cy, key, dataKey, stroke} = props;

  const logoUrl = `https://odds-sport-images-753931602578.s3.amazonaws.com/logos-original/${images[dataKey]}-original.PNG`

  if (key === 'dot-0') {
    return (
      <svg key={key} x={cx - 40} y={cy - 20} width={40} height={40}
        xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">       
        <image xlinkHref={logoUrl} height="40" width="40"/>
      </svg>
    );
  }

  return (
    <svg x={cx - 5} y={cy - 14} height="20" width="20">
      <circle cx="5" cy="15" r="3" stroke={stroke} stroke-width="1" fill="white" />
    </svg>
  )
}

function App() {
  return (
    <div className="App" style={{ margin: '10rem auto' }}>
      <div style={{margin: "0 auto", width: '600px'}}>
        <div style={{ height: '500px', display: "flex", justifyContent: "center", flexDirection: 'column', alignItems: 'flex-start' }}>
          <h3>Line Chart</h3>
          <ResponsiveContainer width={700} height="80%">
            <LineChart data={endData}
                  margin={{top: 5, right: 30, left: 20, bottom: 5}}>
              <XAxis padding={{ left: 50 }} tickLine={false} dataKey="time">
              {/* <Label value="MON 3" offset={0} position="insideBottomLeft" /> */}
              </XAxis>
              <YAxis tickLine={false} axisLine={false} tick={YAxisTick}/>
              <CartesianGrid strokeDasharray="3 3" vertical={false} />
              <Tooltip
                offset={0}
                cursor={{ stroke: '#F79720', strokeWidth: 1 }}
                content={<CustomTooltip />}
              />
              <Line type="linear" dataKey="pv" stroke="#8884d8" dot={CustomizedDot} />
              <Line type="linear" dataKey="uv" stroke="#82ca9d" dot={CustomizedDot} />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
}

export default App;
